package com.example.challengebreakingbadapi.presentation

import androidx.lifecycle.*
import com.example.challengebreakingbadapi.core.Resource
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor
import com.example.challengebreakingbadapi.repository.actor.repository.ActorRepository
import com.example.challengebreakingbadapi.repository.actor.repository.ActorRepositoryImpl
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class ActorViewModel(private val actorRepository: ActorRepository): ViewModel() {

    fun updateActor(actor: Actor) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            actor.isFavorite = !actor.isFavorite
            emit(
                Resource.Success(
                    actorRepository.updateCharacter(actor)
                )
            )
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun fetchGetAllCharacters() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(
                Resource.Success(
                    actorRepository.getAllCharacters()
                )
            )
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun fetchMoreGetAllCharacters(offset: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(
                Resource.Success(
                    actorRepository.getMoreCharacters(
                        limit = 10,
                        offset,
                    )
                )
            )
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class ActorViewModelFactory(private val actorRepositoryImpl: ActorRepositoryImpl): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ActorRepository::class.java).newInstance(actorRepositoryImpl)
    }
}