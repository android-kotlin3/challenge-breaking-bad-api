package com.example.challengebreakingbadapi.aplication

object AppConstants {

    // API
    const val BASE_URL = "https://www.breakingbadapi.com/api/"

    // Get All Characters
    const val GETALLCHARACTER = "characters/"

}