package com.example.challengebreakingbadapi.repository

import androidx.room.TypeConverter
import com.example.challengebreakingbadapi.repository.actor.local.model.Appearances
import com.example.challengebreakingbadapi.repository.actor.remote.model.Occupations
import com.google.gson.Gson

class RoomConverters {
    @TypeConverter
    fun OccupationslistToJson(value: List<Occupations>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToListOccupations(value: String) = Gson().fromJson(value, Array<Occupations>::class.java).toList()

    @TypeConverter
    fun listAppearancesToJson(value: List<Appearances>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToListAppearances(value: String) = Gson().fromJson(value, Array<Appearances>::class.java).toList()
}