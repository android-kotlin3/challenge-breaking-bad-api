package com.example.challengebreakingbadapi.repository.actor.local.dao

import androidx.room.*
import com.example.challengebreakingbadapi.repository.actor.local.model.ActorEntity

@Dao
interface ActorDao {
    @Query("SELECT * FROM ActorEntity")
    suspend fun getAllCharacters(): List<ActorEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveActor(actorEntity: ActorEntity)
}