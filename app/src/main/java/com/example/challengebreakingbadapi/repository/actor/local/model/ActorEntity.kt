package com.example.challengebreakingbadapi.repository.actor.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor

@Entity
data class ActorEntity(
    @PrimaryKey
    val id: Int = -1,
    @ColumnInfo(name = "name")
    val name: String = "",
    @ColumnInfo(name ="birthday")
    val birthday: String = "",
    @ColumnInfo(name ="img")
    val img: String = "",
    @ColumnInfo(name ="status")
    val status: String = "",
    @ColumnInfo(name ="nickname")
    val nickname: String = "",
    @ColumnInfo(name ="portrayed")
    val portrayed: String = "",
    @ColumnInfo(name ="favorite")
    val isFavorite: Boolean = false
)

fun ActorEntity.toActor( ): Actor {
    return Actor(
        this.id,
        this.name,
        this.birthday,
        occupation = listOf<String>(),
        this.img,
        this.status,
        appearance = listOf<Int>(),
        this.nickname,
        this.portrayed,
        this.isFavorite
    )
}

// Extension Function
fun List<ActorEntity>.toActorList(): List<Actor> {
    val resultList = mutableListOf<Actor>()
    this.forEach { actorEntity ->

        resultList.add(actorEntity.toActor())
    }
    return resultList
}