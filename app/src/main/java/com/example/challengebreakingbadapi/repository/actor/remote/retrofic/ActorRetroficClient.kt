package com.example.challengebreakingbadapi.repository.actor.remote.retrofic

import com.example.challengebreakingbadapi.aplication.AppConstants
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ActorRetroficClient {
    val webService: ActorWebService by lazy {
        Retrofit.Builder().baseUrl(AppConstants.BASE_URL)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().create()
                )
            ).build().create(ActorWebService::class.java)
    }
}