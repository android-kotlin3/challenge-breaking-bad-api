package com.example.challengebreakingbadapi.repository.actor.remote.retrofic

import com.example.challengebreakingbadapi.aplication.AppConstants
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor
import retrofit2.http.GET
import retrofit2.http.Query

interface ActorWebService {

    @GET(AppConstants.GETALLCHARACTER)
    suspend fun getAllCharacters(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
    ): List<Actor>

}