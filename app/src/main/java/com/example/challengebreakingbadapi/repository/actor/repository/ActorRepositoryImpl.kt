package com.example.challengebreakingbadapi.repository.actor.repository

import com.example.challengebreakingbadapi.core.InternetCheck
import com.example.challengebreakingbadapi.repository.actor.local.source.LocalActorDataSource
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor
import com.example.challengebreakingbadapi.repository.actor.remote.model.toActorEntity
import com.example.challengebreakingbadapi.repository.actor.remote.source.RemoteActorDataSource

class ActorRepositoryImpl(
    private val actorDataSource: RemoteActorDataSource,
    private val localActorDataSource: LocalActorDataSource
): ActorRepository {

    override suspend fun getAllCharacters(limit: Int, offset: Int): List<Actor> {
        return if(InternetCheck.isNetworkAvailable()){
            if(localActorDataSource.getAllCharacters().isEmpty()){
                actorDataSource.getAllCharacters(
                    limit = limit,
                    offset = offset,
                ).forEach { actor ->
                    localActorDataSource.saveActor(actor.toActorEntity())
                }
            }
            favoriteList(localActorDataSource.getAllCharacters())
        } else {
            favoriteList(localActorDataSource.getAllCharacters())
        }
    }

    private fun favoriteList(actorList: List<Actor>): List<Actor> {
        val listResult = mutableListOf<Actor>()
        actorList.forEach { actor ->
            if(actor.isFavorite){
                listResult.add(actor)
            }
        }
        actorList.forEach { actor ->
            if(!actor.isFavorite){
                listResult.add(actor)
            }
        }
        return listResult
    }

    override suspend fun getMoreCharacters(limit: Int, offset: Int): List<Actor> {
        val listResultPivot = mutableListOf<Actor>()
        actorDataSource.getAllCharacters(
            limit = limit,
            offset = offset,
        ).forEach { actor ->
            listResultPivot.add(actor)
        }
        listResultPivot.forEach { actorExist ->
            if (!localActorDataSource.getAllCharacters().contains(actorExist)) {
                localActorDataSource.saveActor(actorExist.toActorEntity())
            }
        }
        return listResultPivot
    }

    override suspend fun updateCharacter(actor: Actor) {
        localActorDataSource.saveActor(actor.toActorEntity())
    }


}
