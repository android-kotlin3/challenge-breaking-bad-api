package com.example.challengebreakingbadapi.repository.actor.repository

import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor

interface ActorRepository {
    suspend fun getAllCharacters(
        limit: Int = 10,
        offset: Int = 0,
    ): List<Actor>

    suspend fun getMoreCharacters(
        limit: Int = 10,
        offset: Int = 0,
    ): List<Actor>

    suspend fun updateCharacter(actor: Actor)

}