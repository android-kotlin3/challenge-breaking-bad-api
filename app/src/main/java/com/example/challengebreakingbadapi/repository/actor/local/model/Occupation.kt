package com.example.challengebreakingbadapi.repository.actor.remote.model

data class Occupations(val occupation: String)