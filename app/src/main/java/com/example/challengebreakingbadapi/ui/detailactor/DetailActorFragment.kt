package com.example.challengebreakingbadapi.ui.detailactor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.challengebreakingbadapi.R
import com.example.challengebreakingbadapi.core.Resource
import com.example.challengebreakingbadapi.databinding.FragmentDetailActorBinding
import com.example.challengebreakingbadapi.presentation.ActorDetailViewModel
import com.example.challengebreakingbadapi.presentation.ActorViewModel
import com.example.challengebreakingbadapi.presentation.ActorViewModelFactory
import com.example.challengebreakingbadapi.repository.AppDatabase
import com.example.challengebreakingbadapi.repository.actor.local.source.LocalActorDataSource
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor
import com.example.challengebreakingbadapi.repository.actor.remote.retrofic.ActorRetroficClient
import com.example.challengebreakingbadapi.repository.actor.remote.source.RemoteActorDataSource
import com.example.challengebreakingbadapi.repository.actor.repository.ActorRepositoryImpl
import com.example.challengebreakingbadapi.ui.MainActivity

class DetailActorFragment : Fragment(R.layout.fragment_detail_actor) {

    private lateinit var binding: FragmentDetailActorBinding
    private lateinit var mActorDetailViewModel: ActorDetailViewModel
    private val args by navArgs<DetailActorFragmentArgs>()
    private var mActivity: MainActivity? = null

    private val viewModel by viewModels<ActorViewModel> {
        ActorViewModelFactory(
            ActorRepositoryImpl(
                RemoteActorDataSource(ActorRetroficClient.webService),
                LocalActorDataSource(AppDatabase.getDatabase(requireContext()).actorDao())
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActorDetailViewModel = ViewModelProvider(requireActivity()).get(ActorDetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailActorBinding.inflate(inflater, container, false)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailActorBinding.bind(view)
        loadParameters()
        setHasOptionsMenu(true)
    }

    private fun loadParameters() {

        binding.fabDetail.setOnCheckedChangeListener { compoundButton, b ->
            if(binding.fabDetail.isChecked) {
                Log.d("LiveData" , "${mActorDetailViewModel.getActorSelected().value}")
                mActorDetailViewModel.getActorSelected().value?.let { onFavorite(it) }
            }
        }

        Glide.with(requireContext()).load(args.image).centerCrop().into(binding.ivActor)
        binding.tvNickName.text = args.nickmane
        binding.tvOccupations.text = args.occupations
        binding.tvStatus.text = args.status
        binding.tvPortrayed.text = args.portrayed
        binding.fabDetail.isChecked = args.isFavorite
        setupActionBar(args.nickmane)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                mActivity?.onBackPressed()
                true
            } else -> super.onOptionsItemSelected(item)
        }
    }

     private fun onFavorite(actor: Actor) {
        viewModel.updateActor(actor).observe(viewLifecycleOwner, { result ->
            when(result) {
                is Resource.Loading -> {
//                    Log.d("LiveData", "Loading...")
                }
                is Resource.Success -> {
                }
                is Resource.Failure -> {
//                    Log.d("LiveData", "Failure")
                }
            }
        })
    }


    private fun setupActionBar(nickname: String) {
        mActivity = activity as? MainActivity
        mActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mActivity?.supportActionBar?.title = nickname
    }

    override fun onDestroy() {
        mActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mActivity?.supportActionBar?.title = getString(R.string.app_name)
        super.onDestroy()
    }

}