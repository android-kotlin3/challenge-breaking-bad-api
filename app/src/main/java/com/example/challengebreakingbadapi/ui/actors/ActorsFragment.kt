package com.example.challengebreakingbadapi.ui.actors

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.challengebreakingbadapi.R
import com.example.challengebreakingbadapi.core.Resource
import com.example.challengebreakingbadapi.databinding.FragmentActorsBinding
import com.example.challengebreakingbadapi.presentation.ActorDetailViewModel
import com.example.challengebreakingbadapi.presentation.ActorViewModel
import com.example.challengebreakingbadapi.presentation.ActorViewModelFactory
import com.example.challengebreakingbadapi.repository.AppDatabase
import com.example.challengebreakingbadapi.repository.actor.local.source.LocalActorDataSource
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor
import com.example.challengebreakingbadapi.repository.actor.repository.ActorRepositoryImpl
import com.example.challengebreakingbadapi.repository.actor.remote.retrofic.ActorRetroficClient
import com.example.challengebreakingbadapi.repository.actor.remote.source.RemoteActorDataSource
import com.example.challengebreakingbadapi.ui.actors.adapters.ActorAdapter

class ActorsFragment : Fragment(R.layout.fragment_actors), ActorAdapter.OnActorClickListener {

    private val viewModel by viewModels<ActorViewModel> {
        ActorViewModelFactory(
            ActorRepositoryImpl(
                RemoteActorDataSource(ActorRetroficClient.webService),
                LocalActorDataSource(AppDatabase.getDatabase(requireContext()).actorDao())
            )
        )
    }

    private lateinit var binding: FragmentActorsBinding
    private lateinit var mLayoutManager : LinearLayoutManager
    private lateinit var mActorDetailViewModel: ActorDetailViewModel
    private lateinit var mAdapter: ActorAdapter

    private var isLoading: Boolean = false
    private var actorsList = mutableListOf<Actor>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActorDetailViewModel = ViewModelProvider(requireActivity()).get(ActorDetailViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentActorsBinding.bind(view)

        mAdapter = ActorAdapter(actorsList, this@ActorsFragment)
        mLayoutManager = LinearLayoutManager(requireContext())

        binding.rvActors.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mAdapter
        }
        getAllCharacters()
        addScrollListener()
    }


    private fun getAllCharacters() {
        viewModel.fetchGetAllCharacters().observe(viewLifecycleOwner, { result ->
            when(result) {
                is Resource.Loading -> {
//                    Log.d("LiveData", "Loading...")
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
//                    Log.d("LiveData", "Characters: ${result.data}")
                    // 1# I store all the information in a list in the view
                    actorsList.addAll(result.data)
                    // 2# I add the information to the adapter
                    mAdapter.setActors(actorsList)
                    binding.progressBar.visibility = View.GONE
                }
                is Resource.Failure -> {
//                    Log.d("LiveData", "Failure")
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }

    private fun addScrollListener() {
        binding.rvActors.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!isLoading) {
                    if (mLayoutManager.findLastCompletelyVisibleItemPosition() == actorsList.size - 1) {
                        Log.d("LiveData", "Scroll More Please...")
                        getMoreAllCharacters()
                        isLoading = true
                    }
                }
            }

        })
    }

    private fun getMoreAllCharacters() {
        mAdapter.addActor(Actor())

        val currentSize: Int = actorsList.size

        viewModel.fetchMoreGetAllCharacters(currentSize).observe(viewLifecycleOwner, { result ->
            when(result) {
                is Resource.Loading -> {
//                        Log.d("LiveData", "Loading...")
                    isLoading = true
                }
                is Resource.Success -> {
                    mAdapter.removeActor(actorsList.size - 1)
//                        Log.d("LiveData", "Characters: ${result.data}")
                    mAdapter.addMoreActors(currentSize, result.data as MutableList<Actor>)
                    isLoading = false
                }
                is Resource.Failure -> {
//                        Log.d("LiveData", "Failure")
                    isLoading = false
                }
            }
        })
    }


    override fun onActorClick(actor: Actor) {

        val action = ActorsFragmentDirections.actionActorsFragmentToDetailActorFragment(
            nickmane = actor.nickname,
            image = actor.img,
            occupations = getOccupations(actor.occupation),
            status = actor.status,
            portrayed = actor.portrayed,
            isFavorite = actor.isFavorite,
        )

        mActorDetailViewModel.setActorSelected(actor)

        findNavController().navigate(action)
    }

    override fun onFavorite(actor: Actor) {
        viewModel.updateActor(actor).observe(viewLifecycleOwner, { result ->
            when(result) {
                is Resource.Loading -> {
//                    Log.d("LiveData", "Loading...")
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressBar.visibility = View.GONE
                }
                is Resource.Failure -> {
//                    Log.d("LiveData", "Failure")
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }


    private fun getOccupations(oc: List<String>): String {
        var lista = ""
        if(oc.isNotEmpty()){
            for (o in oc){
                lista += "$o ,"
            }
        } else {
            lista = "No Occupations"
        }
        return lista
    }


}