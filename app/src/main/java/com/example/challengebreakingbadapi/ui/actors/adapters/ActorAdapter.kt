package com.example.challengebreakingbadapi.ui.actors.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challengebreakingbadapi.core.BaseViewHolder
import com.example.challengebreakingbadapi.databinding.ActorItemBinding
import com.example.challengebreakingbadapi.databinding.ItemLoadingBinding
import com.example.challengebreakingbadapi.repository.actor.remote.model.Actor

class ActorAdapter(
    private var actorList: MutableList<Actor>,
    private val itemClickListener: OnActorClickListener
): RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnActorClickListener {
        fun onActorClick(actor: Actor)
        fun onFavorite(actor: Actor)
    }

    private var VIEW_TYPE_ITEM: Int = 1;
    private var VIEW_TYPE_LOADING: Int = 0;

    fun setActors(actors: MutableList<Actor>) {
        actorList = actors
        notifyDataSetChanged()
    }

    fun addActor(actor: Actor) {
        actorList.add(actor)
        notifyItemInserted(actorList.size - 1 )
    }

    fun removeActor(index: Int) {
        actorList.removeAt(index)
        notifyItemRemoved(index)
    }

    fun addMoreActors(index: Int, actors: MutableList<Actor>) {
        actorList.addAll(actors)
        notifyItemRangeInserted(index, actors.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return if(viewType == VIEW_TYPE_ITEM){
            val itemBinding = ActorItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            val holder  = ActorViewHolder(itemBinding, parent.context)
            itemBinding.root.setOnClickListener {
                val position = holder.bindingAdapterPosition.takeIf { it != DiffUtil.DiffResult.NO_POSITION } ?: return@setOnClickListener
                itemClickListener.onActorClick(actorList[position])
            }
            holder
        } else {
            val itemBindingLoading = ItemLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            val holder  = ActorLoadingViewHolder(itemBindingLoading, parent.context)
            holder
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when(holder) {
            is ActorViewHolder -> holder.bind(actorList[position])
        }
    }

    override fun getItemCount(): Int = actorList.size

    override fun getItemViewType(position: Int): Int {
        return if(actorList[position].id != -1) {
            VIEW_TYPE_ITEM
        } else {
            VIEW_TYPE_LOADING
        }
    }

    private inner class ActorViewHolder(val binding: ActorItemBinding, val context: Context): BaseViewHolder<Actor>(binding.root) {
        override fun bind(item: Actor) {
            binding.fab.setOnClickListener{
                itemClickListener.onFavorite(item)
                notifyDataSetChanged()
            }
            Glide.with(context).load(item.img).centerCrop().into(binding.imageView)
            binding.tvName.text = item.name
            binding.tvNickName.text = item.nickname
            binding.fab.isChecked = item.isFavorite
        }
    }

    private inner class ActorLoadingViewHolder(val binding: ItemLoadingBinding, val context: Context): BaseViewHolder<Actor>(binding.root) {
        override fun bind(item: Actor) {
            binding.itemProgressBar.visibility = View.VISIBLE
        }
    }

}